export class DestinoViaje{
    nombre:string;
    imagenUrl:String;
    
    constructor(n:string, u:string){
        this.nombre = n;
        this.imagenUrl = u;
    }
}